//
//  GravitionalObject.h
//  MyCppGame
//
//  Created by Oğuz Uzman on 04/10/14.
//
//

#ifndef __MyCppGame__GravitionalObject__
#define __MyCppGame__GravitionalObject__

#include "cocos2d.h"

class GravitionalObject : public cocos2d::Sprite{
public:
    static GravitionalObject* create(std::string filePath, float weight, float speedX, float speedY,float kilometerX, float kilometerY);
    bool init (std::string filePath, float weight, float speedX, float speedY);
    std::string getInstanceName();
    float getWeight();
    void updateSpeed(float forceX, float forceY);
private:
    void update(float dt);
    float speedX, speedY, weight;
    std::string instanceName;
    
};

#endif /* defined(__MyCppGame__GravitionalObject__) */
