//
//  Universe.h
//  MyCppGame
//
//  Created by Oğuz Uzman on 04/10/14.
//
//

#ifndef __MyCppGame__Universe__
#define __MyCppGame__Universe__

#include "cocos2d.h"
#include "GravitionalObject.h"

struct ListNode{
    GravitionalObject * item;
    ListNode * next;
}typedef ListNode ;

class Universe : public cocos2d::Node{
public:
    Universe(float widthInKilometers);
    void update(float dt);
    void addGravitionalObject(GravitionalObject * object);
    
private:
    ListNode * first;
    long gravitionalObjectLength();
    long objectCount = 0 ;
    GravitionalObject * getObjectAtIndex(long index);
    GravitionalObject * objectArray[50];
};


#endif /* defined(__MyCppGame__Universe__) */
