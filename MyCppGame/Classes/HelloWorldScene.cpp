
#include "cocostudio/CocoStudio.h"
#include "HelloWorldScene.h"
#include "Universe.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
   
    
    
    GravitionalObject * obj = GravitionalObject::create("p1.png", 15, 0, 0,100,0);
    obj->setPosition(50, 50);
    GravitionalObject * newObj = GravitionalObject::create("p2.png", 15, 0, 0,0,0);
    newObj->setPosition(512, 300);
    
    GravitionalObject * thirdObj = GravitionalObject::create("p1.png", 15, 0, 0,0,0);
    thirdObj->setPosition(950, 50);
    
    Universe * uni = (new Universe(1000));
    
    uni->addGravitionalObject(obj);
    uni->addGravitionalObject(newObj);
    uni->addGravitionalObject(thirdObj);
    
    this->addChild(uni);
    this->addChild(obj);
    this->addChild(newObj);
    this->addChild(thirdObj);
  
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
