//
//  Universe.cpp
//  MyCppGame
//
//  Created by Oğuz Uzman on 04/10/14.
//
//

#include "Universe.h"
#include <math.h>
#include <iostream>
void Universe::update(float dt){
    long length = gravitionalObjectLength();
    for(long i = 0 ; i < length ; i++){
        
        GravitionalObject * object = getObjectAtIndex(i);
        float forceX = 0 ;
        float forceY = 0 ;
        
        for(long j = 0 ; j < length ; j++){
            if(i!=j){
                GravitionalObject * tempObj = getObjectAtIndex(j);
            float distanceSquared =pow(object->getPositionX()-tempObj->getPositionX(),2) + pow(object->getPositionY() - tempObj->getPositionY(),2) ;
            float force = ( tempObj->getWeight()*object->getWeight() ) / distanceSquared;
            
            
            
            forceX +=force* (  ( ( tempObj->getPositionX() - object->getPositionX() ) / sqrt( distanceSquared ) ) );
            forceY += force*(  ( ( tempObj->getPositionY() - object->getPositionY() ) / sqrt( distanceSquared ) ) );
            }
        }
        
        cocos2d::Size winSize = cocos2d::Director::getInstance()->getWinSize();
        
        
        
        std::cout<<"Object "<< i <<"\n";
        std::cout<<"forceX: "<<forceX<<"\n";
        std::cout<<"forceY: " <<forceY<<"\n";
        object->updateSpeed(forceX, forceY);
        
        int s = 5 ;
        s*=s;
        
        
    }
}

Universe::Universe(float widthInKilometers){
    this->schedule(schedule_selector(Universe::update));
}

void Universe::addGravitionalObject(GravitionalObject * object){
    objectArray[objectCount++]  = object;
}

long Universe::gravitionalObjectLength(){
    return objectCount;
}

GravitionalObject * Universe::getObjectAtIndex(long index){
    return objectArray[index];
}




