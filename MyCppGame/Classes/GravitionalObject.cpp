//
//  GravitionalObject.cpp
//  MyCppGame
//
//  Created by Oğuz Uzman on 04/10/14.
//
//

#include "GravitionalObject.h"


GravitionalObject* GravitionalObject::create(std::string filePath, float weight, float speedX, float speedY,float kilometerX, float kilometerY)
{
    GravitionalObject *sprite = new (std::nothrow) GravitionalObject();
    if (sprite && sprite->init(filePath, weight, speedX, speedY))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

void GravitionalObject::update(float dt){
    
   this->setPosition(getPositionX()+speedX, getPositionY() + speedY);

}
bool GravitionalObject::init (std::string filePath, float weight, float speedX, float speedY){
    bool returnVal = this->initWithFile(filePath);
    this->weight = weight;
    this->speedX = speedX;
    this->speedY = speedY;
    this->schedule(schedule_selector(GravitionalObject::update));
    return returnVal;
}


void GravitionalObject::updateSpeed(float forceX, float forceY){
    
    speedX += forceX/(getWeight());
    speedY += forceY/(getWeight());
    
}

std::string GravitionalObject::getInstanceName(){
    return instanceName;
}

float GravitionalObject::getWeight(){
    return weight;
}